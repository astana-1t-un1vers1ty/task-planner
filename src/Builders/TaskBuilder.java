package Builders;

import Models.Task;
import Models.User;

public class TaskBuilder {
    private String text;
    private String description;

    public TaskBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public TaskBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public Task build() {
        return new Task(text, description);
    }
}
