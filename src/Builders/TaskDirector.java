package Builders;

import Models.Task;

public class TaskDirector {
    private final TaskBuilder builder;

    public TaskDirector(TaskBuilder builder) {
        this.builder = builder;
    }

    public Task createTask(String text, String description) {
        return builder
                .setText(text)
                .setDescription(description)
                .build();
    }
}
