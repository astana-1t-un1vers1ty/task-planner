package Notification;

import Models.Task;
import Models.User;

public interface NotificationSystem {
    void addTask(Task task);
    void removeTask(Task task);

    void addObserver(Task task, User observer);

    void removeObserver(Task task, User observer);
    void notifyObservers(Task task, String msg);
}
