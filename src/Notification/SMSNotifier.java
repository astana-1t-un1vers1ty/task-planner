package Notification;

import Models.Task;
import Models.User;

public class SMSNotifier extends SimpleNotifier {

    @Override
    public void write(Task task, String msg) {
        for(User user:task.getAssignee()){
            new Observer(user).handleEvent(task, msg);
            System.out.println("Sent SMS on number "+user.phoneNumber());
            System.out.println("__________________________________________");
        }
    }
}
