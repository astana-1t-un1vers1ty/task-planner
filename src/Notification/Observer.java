package Notification;

import Models.Task;
import Models.User;

public class Observer implements NotificationObserver {
    private User user;
    public Observer(User user) {
        this.user = user;
    }


    @Override
    public void handleEvent(Task task,String msg) {
        System.out.println(msg+"\n" + task.getText()+"\n" + task.getDescription());
    }
}