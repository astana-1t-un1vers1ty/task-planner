package Notification;

import Models.Task;
import Models.User;

public class EmailNotifier extends SimpleNotifier {


    @Override
    public void write(Task task, String msg) {
        for(User user:task.getAssignee()){
            new Observer(user).handleEvent(task, msg);
            System.out.println("Sent on email "+user.email());
            System.out.println("__________________________________________");
        }
    }
}
