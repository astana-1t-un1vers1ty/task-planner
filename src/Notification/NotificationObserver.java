package Notification;

import Models.Task;

public interface NotificationObserver {
    public void handleEvent(Task task, String msg);
}
