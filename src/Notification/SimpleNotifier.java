package Notification;

import Models.Task;
import Models.User;

import java.util.*;

public class SimpleNotifier implements NotificationSystem{
    private SimpleNotifier nextNotifier;
    ArrayList<Task> tasks = new ArrayList<>();
    @Override
    public void addTask(Task task){
        tasks.add(task);
        notifyObservers(task, "New task!");
    }

    @Override
    public void removeTask(Task task){
        tasks.remove(task);
        notifyObservers(task, "Task deleted!");
    }
    @Override
    public void addObserver(Task task, User observer) {
        if(tasks.contains(task)){
            tasks.remove(task);
            task.setAssignee(observer);
            tasks.add(task);
            System.out.println( "Observer " + observer.email() + " added!");
            System.out.println("__________________________________________");
        }

    }

    @Override
    public void removeObserver(Task task, User observer) {
        if(tasks.contains(task)){
            for(Task t: tasks){
                if(t.getAssignee().contains(observer)){
                    t.removeAssignee(observer);
                    System.out.println( "Observer " + observer.email() + " deleted!");
                    System.out.println("__________________________________________");
                }
            }
        }

    }


    @Override
    public void notifyObservers(Task task,String msg) {
        write(task, msg);
        if(nextNotifier!=null){
            nextNotifier.notifyObservers(task, msg);
        }
    }



    public void setNextNotifier(SimpleNotifier nextNotifier){
        this.nextNotifier = nextNotifier;
    }

    public void write(Task task, String msg){
        for(User user:task.getAssignee()){
            new Observer(user).handleEvent(task, msg);
            System.out.println("Report for admin");
            System.out.println("__________________________________________");
        }
    }

    public void write(Task task, String msg, User user){
        new Observer(user).handleEvent(task, msg);
        System.out.println("__________________________________________");
    }
}
