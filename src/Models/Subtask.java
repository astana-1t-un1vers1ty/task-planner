package Models;

public class Subtask {
    public Subtask(String text) {
        this.text = text;
    }

    private String text;
    private Boolean completed = false;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isCompleted() {
        return completed;
    }

    public void complete(Boolean completed) {
        this.completed = completed;
    }
}
