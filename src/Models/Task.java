package Models;

import java.util.ArrayList;
import java.util.List;

public class Task {
    public Task(String text, String description) {
        this.text = text;
        this.description = description;
    }

    private final String text;
    private final String description;
    private final List<User> assignees = new ArrayList<>();

    public String getText() {
        return text;
    }

    public List<User> getAssignee() {
        return assignees;
    }

    public void setAssignee(User assignee) {
        this.assignees.add(assignee);
    }

    public void removeAssignee(User assignee) {
        this.assignees.remove(assignee);
    }

    public String getDescription() {
        return description;
    }
}
