package Models;

public record User(String fullName, String email, String phoneNumber) { }
