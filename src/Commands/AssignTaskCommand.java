package Commands;

import Models.Task;
import Models.User;

public class AssignTaskCommand extends TaskCommand {
    private final User user;

    public AssignTaskCommand(Task task, User user) {
        this.user = user;
        this.task = task;
    }

    @Override
    public void execute() {
        task.setAssignee(user);
    }

    @Override
    public void rollback() {
        task.removeAssignee(user);
    }
}
