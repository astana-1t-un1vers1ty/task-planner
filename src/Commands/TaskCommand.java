package Commands;

import Models.Task;

public abstract class TaskCommand implements ITaskCommand {
    protected Task task;

    public Task getTask() {
        return task;
    }
}
