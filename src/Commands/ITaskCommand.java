package Commands;

public interface ITaskCommand {
    void execute();
    void rollback();
}
