package Commands;

import Builders.TaskBuilder;

public class CreateTaskCommand extends TaskCommand {
    public CreateTaskCommand(TaskBuilder builder) {
        this.builder = builder;
    }

    private final TaskBuilder builder;

    @Override
    public void execute() {
        task = builder.build();
    }

    @Override
    public void rollback() {
        task = null;
    }
}
