package Commands;

import java.util.Stack;

public class CommandHistory {
    private final Stack<ITaskCommand> previousCommands = new Stack<>();
    private final Stack<ITaskCommand> nextCommands = new Stack<>();

    public void push(ITaskCommand command) {
        previousCommands.push(command);
        nextCommands.clear();
    }

    public ITaskCommand pop() {
        var command = previousCommands.pop();
        nextCommands.push(command);
        return command;
    }

    public ITaskCommand restore() {
        var command = nextCommands.pop();
        previousCommands.push(command);
        return command;
    }

    public Boolean isEmpty() {
        return previousCommands.isEmpty();
    }

    public Boolean canRestore() {
        return !nextCommands.isEmpty();
    }
}
