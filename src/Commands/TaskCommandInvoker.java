package Commands;

public class TaskCommandInvoker {
    private final CommandHistory history = new CommandHistory();

    public void addCommand(ITaskCommand command) {
        command.execute();
        history.push(command);
    }

    public void undo() {
        if (history.isEmpty()) {
            return;
        }
        var command = history.pop();
        if (command != null) {
            command.rollback();
        }
    }

    public void redo() {
        if (!history.canRestore()) {
            return;
        }
        var command = history.restore();
        if (command != null) {
            command.execute();
        }
    }
}
