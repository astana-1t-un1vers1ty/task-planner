import Builders.TaskBuilder;
import Builders.TaskDirector;
import Commands.AssignTaskCommand;
import Commands.CreateTaskCommand;
import Commands.TaskCommandInvoker;
import Models.Task;
import Models.User;

import Notification.EmailNotifier;
import Notification.SimpleNotifier;
import Notification.SMSNotifier;


public class Main {
    public static void main(String[] args) {
        // Create a task
        TaskBuilder builder = new TaskBuilder();
        TaskDirector director = new TaskDirector(builder);
        Task task = director.createTask("Task 1", "Description 1");

        // Create commands
        CreateTaskCommand createCommand = new CreateTaskCommand(builder);
        AssignTaskCommand assignCommand = new AssignTaskCommand(task, new User("User 1", "user2@example.com", "555-555-5555"));

        // Execute commands
        TaskCommandInvoker invoker = new TaskCommandInvoker();
        invoker.addCommand(createCommand);
        invoker.addCommand(assignCommand);
        invoker.undo();
        invoker.redo();

        // Create notification system
        SimpleNotifier notifier = new SimpleNotifier();
        EmailNotifier emailNotifier = new EmailNotifier();
        emailNotifier.setNextNotifier(new SMSNotifier());
        notifier.setNextNotifier(emailNotifier);
        notifier.addTask(task);
        notifier.addObserver(task, new User("User 2", "user3@example.com", "666-666-6666"));
        notifier.removeTask(task);



    }
}